# Generated by Django 2.0.7 on 2018-07-26 17:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('imoveis', '0009_auto_20180726_1536'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imovel',
            name='cep',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
    ]
