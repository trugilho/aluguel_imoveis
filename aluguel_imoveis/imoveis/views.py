from django.shortcuts import render
from .models import Imovel
from django.urls import reverse_lazy
from django.db.models import Q
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from viacep_package import viacep


class ImovelList(ListView):
	model = Imovel
	template_name = 'imoveis/imovel_list.html'

	def get_queryset(self):
		query = self.request.GET.get('q')
		if query:
			d = viacep.ViaCEP(query)
			data = d.getDadosCEP()
			if data:
				#pesquisas por CEP mostram os imóveis do mesmo bairro
				#sendo possível mostrar os da mesma cidade, alterando o 
				#parametro no filter para cidade
				cidade = data['localidade']
				bairro = data['bairro']
				return Imovel.objects.filter(
							Q(bairro=bairro))						
			else:
				return Imovel.objects.filter(
							Q(cidade=query)|
							Q(bairro=query))
		else:
			return Imovel.objects.all()

class ImovelDetail(DeleteView):
	model = Imovel
	template_name = 'imoveis/imovel_detail.html'


class ImovelCreate(CreateView):
	model = Imovel
	template_name = 'imoveis/imovel_form.html'
	fields = ['titulo','valor','descricao','cep','bairro','cidade','logradouro','numero','complemento','uf','foto']
	success_url = reverse_lazy('imovel_list') 


class ImovelUpdate(UpdateView):
	model = Imovel
	fields = ['titulo','valor','descricao','cep','bairro','cidade','logradouro','numero','complemento','uf','foto']
	template_name = 'imoveis/imoveis_update_form.html'
	success_url = 	reverse_lazy('imovel_list') 


class ImovelDelete(DeleteView):
	model = Imovel
	template_name = 'imoveis/imovel_confirm_delete.html'
	success_url = reverse_lazy('imovel_list')