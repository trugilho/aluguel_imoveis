from django.urls import path
from .views import ImovelList
from .views import ImovelDetail
from .views import ImovelCreate
from .views import ImovelUpdate
from .views import ImovelDelete

urlpatterns = [
	path('imovel_list/', ImovelList.as_view(), name='imovel_list'),
	path('imovel_detail/<int:pk>', ImovelDetail.as_view(), name="imovel_detail"),
	path('imovel_create/', ImovelCreate.as_view(), name="imovel_create"),
	path('imovel_update/<int:pk>', ImovelUpdate.as_view(), name="imovel_update"),
	path('imovel_delete/<int:pk>', ImovelDelete.as_view(), name="imovel_delete"),


]