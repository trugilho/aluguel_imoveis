from django.db import models

class Imovel(models.Model):
	titulo = models.CharField(max_length=40)
	valor = models.DecimalField(max_digits=6, decimal_places=2)
	descricao = models.TextField()
	cep = models.CharField(max_length=10, blank=True, null=True)
	bairro = models.CharField(max_length=60, null=True)
	cidade = models.CharField(max_length=70, null=True)
	logradouro = models.CharField(max_length=150, null=True)
	numero = models.IntegerField(null=True, blank=True)
	complemento = models.CharField(max_length=100, null=True, blank=True)
	uf = models.CharField(max_length=30, null=True)
	foto = models.ImageField(null=True, blank=True)

	class Meta():
		verbose_name_plural = "Imóveis"

	def image_url(self):
		if self.foto and hasattr(self.foto, 'url'):
			return self.foto.url

	def __str__(self):
		return self.titulo




